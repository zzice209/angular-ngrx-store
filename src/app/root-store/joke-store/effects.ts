import {Injectable} from '@angular/core';
import {DataService} from '../../services/data.service';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {Action} from '@ngrx/store';
import * as featureActions from './actions';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

@Injectable()
export class JokeStoreEffects {
  constructor(private dataService: DataService, private actions$: Actions) {}

  @Effect()
  loadRequestEffect$: Observable<Action> = this.actions$.pipe(
    ofType<featureActions.LoadRequestAction>(
      featureActions.ActionTypes.LOAD_REQUEST
    ),
    startWith(new featureActions.LoadRequestAction()),
    switchMap(action =>
    this.dataService
      .getJokes()
      .pipe(map(items => new featureActions.LoadSuccessAction({items})
      ),
        catchError(error => of(new featureActions.LoadFailureAction({error})))
      ))
  );
}
